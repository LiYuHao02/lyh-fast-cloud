package com.lyh.common.core.exception;

/**
 * 演示模式异常
 * 
 * @author lyh
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
