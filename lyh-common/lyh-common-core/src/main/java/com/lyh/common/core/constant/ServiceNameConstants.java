package com.lyh.common.core.constant;

/**
 * 服务名称
 * 
 * @author lyh
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "lyh-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "lyh-system";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "lyh-file";
}
