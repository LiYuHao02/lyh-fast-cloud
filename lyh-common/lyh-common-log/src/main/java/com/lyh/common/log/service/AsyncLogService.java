package com.lyh.common.log.service;

import com.lyh.system.domain.SysOperLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.lyh.common.core.constant.SecurityConstants;
import com.lyh.system.api.RemoteLogService;

/**
 * 异步调用日志服务
 * 
 * @author lyh
 */
@Service
public class AsyncLogService
{
    @Autowired
    private RemoteLogService remoteLogService;

    /**
     * 保存系统日志记录
     */
    @Async
    public void saveSysLog(SysOperLog sysOperLog)
    {
        remoteLogService.saveLog(sysOperLog, SecurityConstants.INNER);
    }
}
