package com.lyh.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.lyh.common.security.annotation.EnableCustomConfig;
import com.lyh.common.security.annotation.EnableLyhFeignClients;
import com.lyh.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 定时任务
 * 
 * @author lyh
 */
@EnableCustomConfig
@EnableCustomSwagger2   
@EnableLyhFeignClients
@SpringBootApplication
public class LyhJobApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(LyhJobApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  定时任务模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
    }
}
