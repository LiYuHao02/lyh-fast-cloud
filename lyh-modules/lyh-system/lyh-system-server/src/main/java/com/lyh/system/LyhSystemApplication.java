package com.lyh.system;

import com.lyh.common.security.annotation.EnableCustomConfig;
import com.lyh.common.security.annotation.EnableLyhFeignClients;
import com.lyh.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 系统模块
 * 
 * @author lyh
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableLyhFeignClients
@SpringBootApplication
public class LyhSystemApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(LyhSystemApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  系统模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
    }
}
